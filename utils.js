const axios = require("axios");
require("dotenv/config");
const fs = require("fs");
const path = require("path");

const TOKEN = process.env.GH_TOKEN;

const baseUrl = "https://api.github.com";
const fileBasePath =
  "/home/horlakz/Documents/programming/devops/personal-server/playbooks";

async function util(gh_username, gh_repo, folder, domain, port) {
  // set file name
  const file_name = domain.includes("horlakz.com")
    ? domain
    : domain.split(" ")[0];

  try {
    // get the token from github api
    const res = await axios({
      method: "post",
      url: `${baseUrl}/repos/${gh_username}/${gh_repo}/actions/runners/registration-token`,
      headers: {
        Accept: "application/vnd.github.v3+json",
        Authorization: `Bearer ${TOKEN}`,
        "X-GitHub-Api-Version": "2022-11-28",
      },
    });

    // set var.yml contents
    const ymlContents = `actions_folder: /home/horlakz/sites/${
      folder ? folder : "dev"
    }/${file_name}
github_username: ${gh_username}
github_repo: ${gh_repo}
github_token: ${res.data.token}
file_name: ${file_name}
domain_name: ${domain}  `;

    // set nginx conf contents
    const confContents = `server {

    server_name ${domain};

    location / {
        proxy_pass http://localhost:${port}; # edit to your port
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }   
}
`;

    // create the files
    fs.writeFile(path.join(`${fileBasePath}/vars.yml`), ymlContents, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("vars.yml created successfully");
      }
    });

    fs.writeFile(
      path.join(`${fileBasePath}/nginx-cfg/${file_name}.conf`),
      confContents,
      (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log(`nginx-cfg/${file_name}.conf created successfully`);
        }
      }
    );

    // catch errors
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
}

module.exports = util;
